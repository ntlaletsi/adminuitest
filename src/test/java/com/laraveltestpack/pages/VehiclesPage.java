package com.laraveltestpack.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class VehiclesPage {

    @FindBy(css="[id$='']")
    public WebElement createNewVehicle;
    @FindBy(css="[id$='']")
    public WebElement createVehicleSubmit;

    @FindBy(css="[id$='']")
    public WebElement make;

    @FindBy(css="[id$='']")
    public WebElement model;

    @FindBy(css="[id$='']")
    public WebElement yearOfManufacture;

    @FindBy(css="[id$='']")
    public WebElement transmission;

    @FindBy(css="[id$='']")
    public WebElement fuelType;


}
